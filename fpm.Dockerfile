FROM php:7-fpm

RUN mkdir -p /share/socket/ && \
  mkdir -p /share/socket/ && \ 
  sed -i 's/listen = 127.0.0.1:9000/listen = \/share\/fpm.sock/' /usr/local/etc/php-fpm.d/www.conf && \
  sed -i 's/listen = 9000/listen = \/share\/fpm.sock/' /usr/local/etc/php-fpm.d/zz-docker.conf && \
  sed -i 's:error_log = /proc/self/fd/2:error_log = /dev/stderr:' /usr/local/etc/php-fpm.d/docker.conf && \
  sed -i 's:access\.log = /proc/self/fd/2:access.log = /dev/stderr:' /usr/local/etc/php-fpm.d/docker.conf && \
  sed -i 's/;pm.max_requests = 500/pm.max_requests = 500/' /usr/local/etc/php-fpm.d/www.conf && \
  sed -i 's:;access.log = log/$pool.access.log:access.log = /dev/stderr:' /usr/local/etc/php-fpm.d/www.conf && \
  chown www-data: -R /share
