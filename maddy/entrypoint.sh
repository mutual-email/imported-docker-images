#!/bin/sh

sed -i s/POSTGRES_USER/$POSTGRES_USER/ /etc/maddy.conf
sed -i s/POSTGRES_PASSWORD/$POSTGRES_PASSWORD/ /etc/maddy.conf
sed -i s/POSTGRES_DB/$POSTGRES_DB/ /etc/maddy.conf
sed -i s/POSTGRES_PORT/$POSTGRES_PORT/ /etc/maddy.conf
sed -i s/POSTGRES_HOST/$POSTGRES_HOST/ /etc/maddy.conf

sed -i s/OBJECT_STORAGE_ENDPOINT/$OBJECT_STORAGE_ENDPOINT/g /etc/maddy.conf
sed -i s/OBJECT_STORAGE_ACCESS_KEY/$OBJECT_STORAGE_ACCESS_KEY/g /etc/maddy.conf
sed -i s/OBJECT_STORAGE_SECRET_KEY/$OBJECT_STORAGE_SECRET_KEY/g /etc/maddy.conf
sed -i s/OBJECT_STORAGE_BUCKET/$OBJECT_STORAGE_BUCKET/g /etc/maddy.conf
sed -i s/OBJECT_STORAGE_REGION/$OBJECT_STORAGE_REGION/g /etc/maddy.conf

exec "$@"
