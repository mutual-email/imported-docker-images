# Docker images for Wildduck

> This repo is derived from the configs and docker files here: https://github.com/nodemailer/wildduck-dockerized

At the moment this repo contains an image for Haraka (receiving MTA) and another for ZoneMTA (sending MTA), the two SMTP components that work alongside wildduck. The customisation is light and mostly involves some heavy handed use of sed to interpolate stuff into config files. 
 
