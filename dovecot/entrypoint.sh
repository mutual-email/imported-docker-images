#!/bin/bash

echo "Inserting s3 access key"
sed -i s#ACCESSKEY#$S3ACCESSKEY#g /etc/dovecot/dovecot.conf

echo "inserting s3 secret key"
sed -i s#SECRET#$S3SECRET#g /etc/dovecot/dovecot.conf

echo "inserting s3 location"
sed -i s#S3LOCATION#$S3LOCATION#g /etc/dovecot/dovecot.conf

echo "inserting s3 bucket"
sed -i s#S3BUCKET#$S3BUCKET#g /etc/dovecot/dovecot.conf


echo "inserting postgres user"
sed -i s#POSTGRES_USER#$POSTGRES_USER#g /etc/dovecot/auth.lua

echo "inserting postgres password"
sed -i s#POSTGRES_PASSWORD#$POSTGRES_PASSWORD#g /etc/dovecot/auth.lua

echo "inserting postgres db"
sed -i s#POSTGRES_DB#$POSTGRES_DB#g /etc/dovecot/auth.lua

echo "inserting postgres host"
sed -i s#POSTGRES_HOST#$POSTGRES_HOST#g /etc/dovecot/auth.lua

echo "inserting postgres port"
sed -i s#POSTGRES_PORT#$POSTGRES_PORT#g /etc/dovecot/auth.lua

#echo "inserting token grant url"
#sed -i s#GRANTURL#$GRANTURL#g /etc/dovecot/dovecot-oauth2.plain.conf.ext

#echo "inserting oauth client id"
#sed -i s/OAUTHCLIENTID/$OAUTHCLIENTID/g /etc/dovecot/dovecot-oauth2.plain.conf.ext

#echo "inserting oauth client secret"
#sed -i s/OAUTHCLIENTSECRET/$OAUTHCLIENTSECRET/g /etc/dovecot/dovecot-oauth2.plain.conf.ext

#echo "inserting token inspection url"
#sed -i s#INTROSPECTIONURL#$INTROSPECTIONURL#g /etc/dovecot/dovecot-oauth2.plain.conf.ext

exec "$@"
