-- usage
-- USER_PASSWORD='password' POSTGRES_PASSWORD='password' lua auth.lua

-- dovecot.auth.auth_request.log_info("Starting script")

-- load drivers
local posix = require "posix"
local driver = require "luasql.postgres"
local argon2 = require "argon2"

-- local pass = [[]] -- initialise as a literal string, otherwise backslashes etc in passwords break the verification!

function db()
  local postgres_password = [[POSTGRES_PASSWORD]]
  local postgres_user = [[POSTGRES_USER]]
  local postgres_host = [[POSTGRES_HOST]]
  local postgres_db = [[POSTGRES_DB]]
  local postgres_port = [[POSTGRES_PORT]]

  env = assert(driver.postgres())
  conn = assert(env:connect(postgres_db,postgres_user,postgres_password,postgres_host,postgres_port))
  return conn
end

function auth_userdb_lookup(req)
  conn = db()
  -- select user
  result = conn:execute(string.format("SELECT uid_lower from oc_users WHERE uid_lower = '%s';",req.user))
  -- 
  row = result:fetch({},"a")

  if not (row.uid_lower == nil or row.uid_lower == '') then
    return dovecot.auth.USERDB_RESULT_OK, "uid=vmail gid=vmail"
  else
    return dovecot.auth.USERDB_RESULT_USER_UNKNOWN, "No such user"
  end
end

function auth_passdb_lookup(req)
  conn = db()
  result = conn:execute(string.format("SELECT uid_lower, password from oc_users where uid_lower = '%s';", req.user))
  row = result:fetch({},"a")

  while row do
    hash = split(row.password,'|')[2]
    salt = split(row.password,'$')[5]
    check, err = argon2.verify(hash,req.password)
 --   dovecot.auth_request.log_debug(row.password)
    if err then
      return dovecot.auth.PASSDB_RESULT_USER_UNKNOWN, ""
    elseif check then
      return dovecot.auth.PASSDB_RESULT_OK, "password=" .. req.password
    end
    row = result:fetch (row, "a")
  end
  return dovecot.auth.PASSDB_RESULT_USER_UNKNOWN, "argon2 verify failed"
end

function split (inputstr, sep)
  if sep == nil then
    sep = "%s"
  end
  local t={}
  for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
    table.insert(t, str)
  end
  return t
end
