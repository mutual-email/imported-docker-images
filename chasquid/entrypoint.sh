#!/bin/bash

echo "Entrypoint starting"

go run /certgen.go

cp fullchain.pem /etc/chasquid/certs/mx.mutual.email/fullchain.pem
cp privkey.pem /etc/chasquid/certs/mx.mutual.email/privkey.pem

chasquid-util print-config

exec "$@"
