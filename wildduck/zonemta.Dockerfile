FROM nodemailer/zonemta-wildduck

COPY zonemta.entrypoint.sh /entrypoint.sh

COPY zonemta.dbs-production.toml /app/config/dbs-production.toml

COPY zonemta.toml /app/config/zonemta.toml

RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]

CMD ["/sbin/tini", "--", "node", "index.js", "--config=config/zonemta.toml"]
