FROM nodemailer/wildduck-webmail:latest
COPY webmail.entrypoint.sh /entrypoint.sh
RUN chmod +x /entrypoint.sh
ENTRYPOINT ["/entrypoint.sh"]
CMD ""
