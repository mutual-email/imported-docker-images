FROM nodemailer/haraka-wildduck

COPY haraka.entrypoint.sh /entrypoint.sh

COPY haraka.plugins /app/config/plugins

RUN chmod +x /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]
CMD ["/sbin/tini", "--", "node", "haraka.js"]
