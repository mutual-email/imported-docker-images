#!/bin/sh

ls /app/config/

/bin/sed -i s_mongo=\"mongodb://127.0.0.1:27017/wildduck\"_mongo=\"mongodb://$MONGODB_HOSTNAME:27017/wildduck\"_ /app/config/dbs-production.toml
/bin/sed -i s_redis=\"redis://127.0.0.1:6379/8\"_redis=\"redis://$REDIS_HOSTNAME:6379/8\"_ /app/config/dbs-production.toml

exec "$@"
